CXX=g++
CFLAGS=-Wall

test: test_main.o sort.o
	$(CXX) sort.o test_main.o test_sort.cpp $(CFLAGS) -o sort_test
	./sort_test

sort.o: sort.cpp
	$(CXX) sort.cpp $(CFLAGS) -c -o sort.o

test_main.o: test_main.cpp
	$(CXX) test_main.cpp $(CFLAGS) -c -o test_main.o

clean:
	rm -f *.o sort_test

.PHONY: clean test
