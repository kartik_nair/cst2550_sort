/*  
    why does this file exist?
    to improve compile speed when testing:
    https://github.com/catchorg/Catch2/blob/v2.x/docs/slow-compiles.md
*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
